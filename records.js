var db = require("./db.js");
exports.get = function (app, req, res) {
    db.query('SELECT * FROM records WHERE app = $1', [app], function (err, result) {
        if (err) {
            res.status(500);
            res.json({ err: 'A1' });
            return console.error(err);
        }
        if (result.rows.length == 0) {
            res.status(404);
        }
        res.json(result.rows);
    });
};
exports.insert = function (app, req, res) {
    // Insert record data for app
    //list.push( { "player":  record.player, "score": record.score, "data": date() } ) ;
    if (!req.body.player || !req.body.score) {
        res.status(422);
        res.json({ err: 'A3' });
        return console.error(req.body);
    }
    db.query('INSERT INTO records(app, player, score) VALUES($1, $2, $3)', [app, req.body.player, req.body.score], function (err, result) {
        if (err) {
            res.status(500);
            res.json({ err: 'A2' });
            return console.error(err);
        }
        res.json({ result: "OK" });
    });

};