var express = require('express'); var app = express(); var records = require("./records.js");
var gameapps = require("./gameapps.js"); var bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.get('/', function (req, res) {
    console.log('GET /');
    gameapps.getAll(req, res)
});

app.get('/:gameapp', function (req, res) {
    console.log('GET /gameapp', records);
    records.get(req.params.gameapp, req, res);
});

app.post('/:gameapp', function (req, res) {
    console.log('POST /gameapp');
    records.insert(req.params.gameapp, req, res);
});

app.listen(process.env.PORT || 8000);
console.log('Servidor up');